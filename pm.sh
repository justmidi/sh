#!/bin/sh

OS=`cat $SH_CONFIG/os`

[ $OS == "alpine" ] && \
    alias install='apk add' && \
    alias uninstall='apk remove' && \
    alias update='apk update'

[ $OS == "void" ] && \
    alias install='xbps-install -Sy' && \
    alias installed='xbps-query -m' && \
    alias uninstall='xbps-remove -Ry' && \
    alias search='xbps-query -Rs' && \
    alias update='xbps-install -Syu'

[ $OS == "debian" ] && \
    alias install='apt install' && \
    alias uninstall='apt remove' && \
    alias update='apt update && apt upgrade -y'
