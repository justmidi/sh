#!/bin/sh

alias cache='cd $XDG_CACHE_HOME'
alias config='cd $XDG_CONFIG_HOME'
alias share='cd $XDG_DATA_HOME'
alias dev='cd "$HOME"/Dev/'
snd(){
    mkdir /tmp/"$1"
    cd /tmp/"$1"
}

alias b=$BROWSER
alias v=$EDITOR

alias p="printf"

alias bookmarks='v Media/Docs/misc/bookmarks.md'

alias desk="$XDG_CONFIG_HOME"/screenlayout/desk.sh
alias mobile="$XDG_CONFIG_HOME"/screenlayout/mobile.sh
alias tv="$XDG_CONFIG_HOME"/screenlayout/tv.sh

alias gpg="gpg2 --homedir $GNUPGHOME "
alias gpg-agent="gpg-agent --homedir $GNUPGHOME "
alias gpg-connect-agent="gpg-connect-agent --homedir $GNUPGHOME "
alias gpgconf="gpgconf --homedir $GNUPGHOME "
alias gpgsm="gpgsm --homedir $GNUPGHOME "
alias gpgv="gpgv2 --homedir $GNUPGHOME "
alias minecraft="flatpak run com.mojang.Minecraft"
#alias ssh="ssh -F $SSH_CONFIG -i $SSH_ID "
#alias ssh-copy-id="ssh-copy-id -i $SSH_ID "
alias wal='$XDG_CONFIG_HOME/sh/wal.sh'
alias xinit="xinit $XINITRC"
alias xrdb="xrdb -load $XRESOURCES \
    ; grep -v '^color' $XDG_CONFIG_HOME/kitty/kitty.conf \
    > /tmp/kitty \
    ; grep 'color' $XRESOURCES \
    | sed s/^\*color/color/g \
    | sed s/:// \
    >> /tmp/kitty \
    ; mv /tmp/kitty $XDG_CONFIG_HOME/kitty/kitty.conf"
