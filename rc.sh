#!/bin/sh

export SH_CONFIG="$HOME"/.config/sh
source $SH_CONFIG/variables.sh
source $SH_CONFIG/shortcuts.sh
source $SH_CONFIG/system_tools.sh
source $SH_CONFIG/clean.sh
source $SH_CONFIG/media.sh
source $SH_CONFIG/weather.sh
source $SH_CONFIG/color.sh
source $SH_CONFIG/pm.sh
