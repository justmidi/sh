#!/bin/sh

alias .='pwd && ls'
alias ..='cd ..'
alias clip='xclip -selection c'
alias df='df -h'
alias du='du -ch'
#alias extract
alias free='free -h'
alias publicIp='curl ipinfo.io/ip'
alias publicHostname='curl ipinfo.io/hostname'
alias ll='ls -l'
alias l='ls -Al'
alias l1='ls -1'
alias la='ls -A'
alias rm='rm -rf'
alias k='loadkeys colemak'
alias homeSize='la | wc -l'
deformat(){
    sed 's/\x1B[@A-Z\\\]^_]\|\x1B\[[0-9:;<=>?]*[-!"#$%&'"'"'()*+,.\/]*[][\\@A-Z^_`a-z{|}~]//g'
}
pastebin(){
    curl -F'file=@'$1 https://0x0.st
}
