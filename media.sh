#!/bin/sh

alias mbsync="mbsync -c ~/.config/isync/mbsyncrc -a"

bgmpv(){
    xwinwrap -ov \
        -st \
        -s \
        -nf \
        -b \
        -un \
        -argb \
        -fs \
        -fdt \
        -o 1.0 \
        -d \
        -debug \
        -- mpv "$1" \
            -wid WID \
            --no-osc \
            --no-osd-bar \
            --player-operation-mode=cplayer \
            --panscan=1.0
}


#alias yt="~/.config/fish/media/yt.sh"
#alias dyt="~/.config/fish/media/dyt.sh"
#alias yts   # search
#alias dytp  # download playlist
#alias ayt   # audio
#alias adyt  # audio download
#alias adytp # audio download playlist
#alias vyt   # video
#alias vdyt  # video download
#alias vdytp # video download playlist
