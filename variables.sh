#!/bin/sh

export BROWSER=firefox
export COLUMNS=80
export EDITOR=nvim
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8
export MAIL="$HOME"/Media/Mail
export MANPAGER='nvim +Man! -'
export PAGER='nvimpager'
export PATH
export PRINTER
export TERM="xterm-kitty"
export TMPDIR=/tmp

export XDG_CACHE_HOME="$HOME"/.cache
export XDG_CONFIG_HOME="$HOME"/.config
export XDG_CONFIG_DIRS="$XDG_CONFIG_HOME":/etc/xdg
export XDG_DATA_HOME="$HOME"/.local/share
export XDG_DATA_DIRS="$XDG_DATA_DIRS":/usr/local/share:/usr/share

export ANDROID_SDK_HOME="$XDG_CONFIG_HOME"/android
export ANDROID_AVD_HOME="$XDG_DATA_HOME"/android/
export ANDROID_EMULATOR_HOME="$XDG_DATA_HOME"/android/
export ADB_VENDOR_KEY="$XDG_CONFIG_HOME"/android
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GPG_AGENT_INFO="$XDG_DATA_HOME"/"S.gpg-agent:$(pgrep gpg-agent):1"
export GPG_TTY=$(tty)
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
export STACK_ROOT="$XDG_DATA_HOME"/stack
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
export SSH_CONFIG="$XDG_CONFIG_HOME"/ssh/config
#export SSH_ID="$XDG_CONFIG_HOME"/ssh/id_ed25519
export WEECHAT_HOME="$XDG_CONFIG_HOME"/weechat
export WGETRC="$XDG_CONFIG_HOME"/wgetrc
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc
export XRESOURCES="$XDG_CONFIG_HOME"/X11/xresources
