#!/bin/sh

wallpapers(){
    directories=`ls -d -1 ~/Media/Images/lukesmithxyz/*/ \
        | sed -e 's/\///g' \
        | sed -e 's/.*lukesmithxyz//'`
    for i in $directories
    do
        echo "        "${i}
    done
}

usage(){
    echo "Usage: wal [options] <wallpapers>

    DESCRIPTION
        Sets wallpapers and theme.

        Wallpapers can either be a file, directory, or one of the 
        genres listed below in the "WALLPAPERS" section. Also,
        because any of the "genres" are directories means you can
        use them as relative paths to any file in those directories.

        For example: \`wal Cityscapes/cityscape_23.png\`

    OPTIONS
        -h | --help)    Displays this help message

        -c | --center)  Set as centered desktop background

        -f | --fill)    Like --scale, but preserves aspect ratio by
                        zooming the image until it fits. May cut off
                        corners

        -m | --max)     Like --fill, but scale the image to the
                        maximum size that fits the screen with black
                        borders on one side

        -p | --print    Prints the current location to the background.

        -r | --reload   Reloads the color palette with the current 
                        background.

        -s | --scale)   Set as scaled desktop background. This will
                        fill the whole background, but the images'
                        aspect ratio may not be preserved

        -t | --tile)    Set as tiled desktop background

    WALLPAPERS"
    wallpapers
}

set_wallpaper(){
    # FR: config file for different image locations
    location="/ . ~/Media/Images ~/Media/Images/lukesmithxyz"
    image_types="png jpg jpeg webp"
    for l in $location; do
        if [ -e $l/$pic ] \
            || [ -d $l/$pic ]; then
            wal -n -i $l/$pic
        else
            for i in $image_types; do
                [ -e $l/$pic.$i ] && wal -n -i $l/$pic.$i
            done
        fi
    done
    feh --no-fehbg $option  "$(< "${HOME}/.cache/wal/wal")"
}

option="--bg-fill"
options(){
    args=`getopt \
        -o 'hc:f:m:prs:t:' \
        --long 'help,center:,fill:,max:,print,reload,scale:,tile:' \
        -- "$@"`
    valid_args=$?; [ $valid_args != "0" ] && usage
    eval set -- "$args"
    while :
    do
        case $1 in
            -h | --help)    usage \
                            ; shift \
                            ;;
            -c | --center)  option="--bg-center" \
                            && pic="$2" \
                            && set_wallpaper \
                            ;  shift 2 \
                            ;;
            -f | --fill)    option="--bg-fill" \
                            && pic="$2" \
                            && set_wallpaper \
                            ; shift 2 \
                            ;;
            -m | --max)     option="--bg-max" \
                            && pic="$2" \
                            && set_wallpaper \
                            ; shift 2 \
                            ;;
            -p | --print)   cat ~/.cache/wal/wal \
                            && echo "" \
                            ; shift \
                            ;;
            -r | --reload)  option="--bg-fill" \
                            && pic="$(cat ~/.cache/wal/wal)" \
                            && set_wallpaper \
                            ; shift \
                            ;;
            -s | --scale)   option="--bg-scale" \
                            && pic="$2" \
                            && set_wallpaper \
                            ; shift 2 \
                            ;;
            -t | --tile)    option="--bg-tile" \
                            && pic="$2" \
                            && set_wallpaper \
                            ; shift 2 \
                            ;;
            --)             shift \
                            ; break \
                            ;;
            *)              echo -e "Error: Unexpected option $1\n" \
                            ; usage \
                            ; exit 2 \
                            ;;
        esac
    done
}

[ $# == 1 ] \
    && ([ $@ == "--help" ] \
        || [ $@ == "-h" ] \
        || [ $@ == "--print" ] \
        || [ $@ == "-p" ] \
        || [ $@ == "--reload" ] \
        || [ $@ == "-r" ]) \
    && options $@ \
    && exit

[ $# == 1 ] \
    && pic="$1" \
    && set_wallpaper $option $@ \
    && exit

[ $# != 0 ] \
    && options $@ \
    && exit

[ $# == 0 ] \
    && echo -e "Error: Arguments required\n" \
    && usage \
    && exit 1
